interface Entity{
    id: string;
}
interface Named{
    name: string;
}

/**
 * Represents Spotify album
 * https://developer.spotify.com/web-api/object-model/#album-object-full
 */
export interface Album extends Entity, Named {
    images: AlbumImage[];
    /**
     * Artist doc
     */
    artists?: Artist[];
}

interface Artist extends Entity, Named{ }

export interface AlbumImage{
    url:string ;
    width:number;
    height:number;
}