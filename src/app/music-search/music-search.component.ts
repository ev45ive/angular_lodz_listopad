import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-music-search',
  template: `
    <div class="row">
      <div class="col">
        <app-search-form></app-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <app-albums-list></app-albums-list>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
