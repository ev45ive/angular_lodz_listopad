import { Component, OnInit, Input } from '@angular/core';
import { Album, AlbumImage } from './album';

@Component({
  selector: 'app-album-item',
  template: `
    <img [src]="image.url" class="card-img-top">
    <div class="card-body">
      <h4 class="card-title">{{album.name}}</h4>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album')
  set onAlbum(album){
    this.album = album
    this.image = album.images[0]
  }

  album:Album

  image:AlbumImage

  // ngOnChanges(changes){
  //   console.log(changes)
  // }

  constructor() { }

  ngOnInit() {
  }

}