import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormArray,
  ValidatorFn, Validators, ValidationErrors, AsyncValidatorFn
} from '@angular/forms';

// import 'rxjs/Rx'
// import 'rxjs/add/operator/filter'
import { filter, debounceTime, distinctUntilChanged } from 'rxjs/operators'
import { MusicService } from './music.service';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/operators/combineLatest';
import { zip } from 'rxjs/operators/zip';
import { withLatestFrom } from 'rxjs/operators/withLatestFrom';

@Component({
  selector: 'app-search-form',
  template: `
    <form class="input-group" [formGroup]="queryForm">
      <input type="text" class="form-control" formControlName="query">
    </form>
    <div *ngIf="queryForm.pending">Please wait...</div>
    <ng-container *ngIf="queryForm.get('query').touched || queryForm.get('query').dirty">
      <div *ngIf="queryForm.get('query').hasError('required')">
        Field is required
      </div>
      <div *ngIf="queryForm.get('query').hasError('minlength')">
        Value is too short. {{ queryForm.get('query').getError('minlength').actualLength }}
      </div>
      <div *ngIf="queryForm.get('query').hasError('censor')">
        Cant use word {{ queryForm.get('query').getError('censor') }}
      </div>
    </ng-container>
    <hr>
  `,
  styles: [`
    form .ng-invalid.ng-dirty,
    form .ng-invalid.ng-touched{
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  constructor(private musicService:MusicService) {

    const censor = (badword):ValidatorFn => (control:AbstractControl) => {
      let hasError = (control.value.indexOf(badword) !== -1)
      return hasError? {
        'censor': badword
      } : null
    }

    const fieldsMatch = firstFieldName => secondField => {
      let firstField = secondField.root.get(firstFieldName)

      return firstField.value == secondField.value ? null : {
        fieldsmismatch: true
      }
    }
  
    const asyncCensor = (badword):AsyncValidatorFn => (control:AbstractControl) => {
      return Observable.create( (observer) => {
        // this.http.get(...).map(reps => {} : null)
        setTimeout(()=>{
          let hasError = (control.value.indexOf(badword) !== -1)
          observer.next(hasError? { 'censor': badword } : null)
          observer.complete()
        },1000)
      })
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('',[
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ],[
        asyncCensor('batman')
      ])
    })

    console.log(this.queryForm)

    this.musicService.query$.subscribe(query => {
      this.queryForm.get('query').setValue(query)
    })
  }

  ngOnInit() {
    let query = this.queryForm.get('query')

    let value$ = query.valueChanges
    let status$ = query.statusChanges

    status$.pipe(
      debounceTime(400),
      filter(s => s == "VALID"),
      withLatestFrom(value$, (valid, value)=>value),
      distinctUntilChanged()
    )
    .subscribe(query => this.search(query) )
  }

  search(query){
    this.musicService.search(query)
  }

}
