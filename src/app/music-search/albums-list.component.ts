import { Component, OnInit, Inject } from '@angular/core';
import { Album } from './album';
import { MusicService } from './music.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-albums-list',
  template: `
    <p *ngIf="(query$ | async) as query">
      Results for query : {{ query }}
    </p>
    <div class="card-group">
    <app-album-item class="card" 
    [album]="album"
    *ngFor="let album of albums$ | async"></app-album-item>
    </div>
  `,
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  albums$:Observable<Album[]>
  query$

  constructor(private musicService:MusicService) {
  }

  ngOnInit() {
    this.query$ = this.musicService.query$
    this.albums$ = this.musicService.getAlbums()
                      // .pipe(map(albums => albums.slice(0,3)))
  }
}

