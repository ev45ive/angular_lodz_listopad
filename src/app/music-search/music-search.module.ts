import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { MusicService } from './music.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumItemComponent
  ],
  exports: [
    MusicSearchComponent
  ],
  providers: [
    // { provide: 'url', useValue: 'http://localhost/' },
    // {
    //   provide: 'urlFactory', useFactory: function (url) {
    //     return function(params){
    //       return url + params
    //     }
    //   }, deps: ['url']
    // },
    // { provide: AbstractMusicService, useClass: MusicService },
    // { provide: MusicService, useClass: MusicService },
    MusicService
  ]
})
export class MusicSearchModule { }
