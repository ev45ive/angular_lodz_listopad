import { Injectable } from '@angular/core';
import { Album } from './album';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AuthService } from '../auth/auth.service';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { map, switchMap, startWith, filter } from 'rxjs/operators';
import { catchError } from 'rxjs/operators/catchError';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of'

@Injectable()
export class MusicService {

  query$ = new BehaviorSubject<string>('')

  albums$ = new BehaviorSubject<Album[]>([])

  search(query) {
    this.query$.next(query)
  }

  getAlbums() {
    return this.albums$
  }

  constructor(
    protected http: HttpClient,
    private auth: AuthService
  ) {

    this.query$.pipe(
      filter(query => query.length >= 3),
      map(query => `https://api.spotify.com/v1/search?q=${query}&type=album`),
      switchMap(url => this.http.get(url, {
        headers: new HttpHeaders({
          Authorization: `Bearer ${this.auth.getToken()}`
        })
      })),
      map(response => response['albums'].items),
      catchError( (err, caught) => {
        if(err.status == 401){
          this.auth.authorize()
        }
        return Observable.of([])
      })
    )
      .subscribe(albums => {
        this.albums$.next(albums)
      })
  }
}
