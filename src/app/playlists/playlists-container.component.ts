import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-playlists-container',
  template: `
      <app-items 
      [playlists]="playlists$ | async"
      [selected]="selected$ | async"
      (selectedChange)="select($event)"
    ></app-items>
  `,
  styles: []
})
export class PlaylistsContainerComponent implements OnInit {
  playlists$
  selected$

  constructor(private playlistsService:PlaylistsService,
              private route:ActivatedRoute,
              private router:Router) { 
  }

  select(playlist){
    this.router.navigate(['/playlists',playlist.id])
  }

  ngOnInit() {
    this.playlists$ = this.playlistsService.getPlaylists()

    this.selected$ = this.route.firstChild.params.pipe(
      tap( params => console.log(params)),
      map( params => params['id']),
      switchMap(id => this.playlistsService.getPlaylist(id)))
  }

}
