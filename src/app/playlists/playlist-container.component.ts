import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlist';
import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-container',
  template: `
    <app-details *ngIf="(selected$ | async) as selected"
    (saved)="save($event)" 
                  [playlist]="selected"></app-details>

    <div *ngIf="!selected">Please select playlist</div>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {
  selected
  selected$

  constructor(private playlistsService: PlaylistsService,
    private route: ActivatedRoute) { }

  save(playlist) {
    this.playlistsService.save(playlist)
    .subscribe((playlist)=>{
      this.playlistsService.getPlaylist(playlist['id'])
    })
  }

  ngOnInit() {
    // this.route.snapshot.params['id']

    this.selected$ = this.route.params.pipe(
      map(params => params['id']),
      switchMap(id => this.playlistsService.getPlaylist(id))
    )
  }

}
