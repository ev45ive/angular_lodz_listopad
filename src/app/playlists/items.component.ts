import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'app-items',
  template: `
    <div class="list-group">
    <div class="list-group-item" 
        *ngFor="let playlist of playlists; let i = index"
        [ngClass]="{ 'active': selected == playlist }"
        (click)="select(playlist)">
        {{(i+1)}}. {{playlist.name}}
      </div>
    </div>
  `,
  // inputs:['playlists:items']
  encapsulation: ViewEncapsulation.Emulated,
  styles: []
})
export class ItemsComponent implements OnInit {

  @Input()
  playlists: Playlist[]

  @Input()
  selected: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}
