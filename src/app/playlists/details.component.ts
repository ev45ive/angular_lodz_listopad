import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'app-details',
  template: `
  <div class="card" *ngIf="mode =='show'">
    <div class="card-body">
      <div class="form-group">
        <label>Name</label>
        <div class="form-control-static">
          {{playlist.name}}
        </div>
      </div>
      <div class="form-group">
        <label>Favourite</label>
        <div class="form-control-static">
          {{playlist.favourite? 'Yes':'No'}}
        </div>
      </div>
      <div class="form-group">
        <label>Color</label>
        <div class="form-control-static" [style.color]="playlist.color">
          {{playlist.color}}
        </div>
      </div>
      <button (click)="edit()">Edit</button>
    </div>
  </div>
 
  <div class="card" *ngIf="mode =='edit'">
    <div class="card-body">
      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" 
          [(ngModel)]="playlist.name">
      </div>
      <div class="form-group">
        <label>Favourite</label>
        <input type="checkbox" 
          [(ngModel)]="playlist.favourite">
      </div>
      <div class="form-group">
        <label>Color</label>
        <input type="color" 
          [(ngModel)]="playlist.color">
      </div>
      <button (click)="save()">Save</button>
    </div>
  </div>

  `,
  styles: []
})
export class DetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  @Output()
  saved = new EventEmitter<Playlist>()

  // show | edit
  mode = 'show'

  save(){
    this.saved.emit(this.playlist)
    this.mode = 'show'
  }

  edit(){
    this.mode = 'edit'
  }

  constructor() { }

  ngOnInit() {
  }

}
