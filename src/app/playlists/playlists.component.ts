import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'app-playlists',
  template: `
  <!-- .row>.col*2 -->
  <div class="row">
    <div class="col">
      <app-playlists-container></app-playlists-container>
    </div>
    <div class="col">
      <router-outlet></router-outlet>
    </div>
  </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  constructor() { }
  
  ngOnInit() {
  }

}
