import { Injectable } from '@angular/core';
import { Playlist } from './playlist';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of'
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { tap } from 'rxjs/operators';

@Injectable()
export class PlaylistsService {

  playlists$ = new BehaviorSubject([])

  constructor(private http:HttpClient) { }

  getPlaylists(){
    this.http.get<Playlist[]>('http://localhost:3000/playlists')
    .subscribe(playlists => this.playlists$.next(playlists))

    return this.playlists$
  }

  save(playlist){
    let request
    if(playlist.id){
      request = this.http.put('http://localhost:3000/playlists/'+playlist.id,playlist)
    }else{ 
      request = this.http.post('http://localhost:3000/playlists/',playlist)
    }
    return request.pipe(
      tap(()=> this.getPlaylists())
    )
  }

  getPlaylist(id:number){
    return this.http.get('http://localhost:3000/playlists/'+id)
  }

}
