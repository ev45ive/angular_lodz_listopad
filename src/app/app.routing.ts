import { RouterModule, Routes } from '@angular/router'
import { PlaylistsComponent } from './playlists/playlists.component';
import { MusicSearchComponent } from './music-search/music-search.component';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';

const routes: Routes = [
    { path: '', redirectTo: 'playlists', pathMatch: 'full' },
    {
        path: 'playlists', component: PlaylistsComponent, children: [
            { path: '', component: PlaylistContainerComponent },
            { path: ':id', component: PlaylistContainerComponent }
        ]
    },
    { path: 'music', component: MusicSearchComponent },
    { path: '**', redirectTo: 'playlists', pathMatch: 'full' }
]

export const Routing = RouterModule.forRoot(routes, {
    // enableTracing: true,
    useHash: true,
    // errorHandler: (err) => {
    //     console.error(err)
    // }
})