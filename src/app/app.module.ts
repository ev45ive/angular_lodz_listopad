import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ItemsComponent } from './playlists/items.component';
import { ItemComponent } from './playlists/item.component';
import { DetailsComponent } from './playlists/details.component';
import { FormsModule } from '@angular/forms'
import { MusicSearchModule } from './music-search/music-search.module';
import { AuthService } from './auth/auth.service';
import { Routing } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { PlaylistsContainerComponent } from './playlists/playlists-container.component';
import { PlaylistsService } from './playlists/playlists.service';

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    ItemsComponent,
    ItemComponent,
    DetailsComponent,
    PlaylistContainerComponent,
    PlaylistsContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Routing,
    MusicSearchModule
  ],
  providers: [
    AuthService,
    PlaylistsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private auth:AuthService){
    this.auth.getToken()
  }
}
