import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  token = ''

  constructor() { }

  authorize(){
    sessionStorage.removeItem('token')

    let client_id = '38bdb23b7cb94bf29db0372558c06c93',
        redirect_uri = 'http://localhost:4200/'

    let url = `https://accounts.spotify.com/authorize?`
            + `response_type=token&client_id=${client_id}&redirect_uri=${redirect_uri}`

    location.replace(url)
  }

  getToken(){
    this.token = JSON.parse(sessionStorage.getItem('token'))

    if(!this.token){
      let match = location.hash.match(/access_token=([^&]*)/)
      this.token = match && match[1]
      location.hash = ''
    }

    if(!this.token){
      this.authorize()
      return 
    }
    sessionStorage.setItem('token', JSON.stringify(this.token))

    return this.token
  }

}
